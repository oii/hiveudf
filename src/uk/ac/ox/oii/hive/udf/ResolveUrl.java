package uk.ac.ox.oii.hive.udf;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import org.apache.hadoop.hive.ql.exec.Description;


@Description(name="ResolveUrl",
value="_FUNC_(href(STRING), base(STRING), url(STRING)) - convert a relative link to a absolute link given the a page base (optional) and the url of the containing the link",
extended = "Returns NULL if url is invalid.\nExample:\n   > SELECT _FUNC_('../',NULL,'http://www.example.com/something/'); \n 'http://www.example.com/'"		
)

public class ResolveUrl {
	
	public ResolveUrl() {
		super();
	}

	private static final Logger LOG =
			Logger.getLogger(ResolveUrl.class.getName());
	
	//Copied from Internet Archive
	
	private URL baseURL;
	private String lastBase;
	
	private boolean isAbsolute(String url) {
		return url.startsWith("http://")
			|| url.startsWith("https://")
			|| url.startsWith("ftp://")
			|| url.startsWith("feed://")
			|| url.startsWith("mailto:")
			|| url.startsWith("mail:")
			|| url.startsWith("javascript:")
			|| url.startsWith("rtsp://");
	}

	private String resolve(String base, String rel) {
		URL absURL = null;
		if(lastBase != null) {
			if(lastBase.equals(base)) {
				try {
					absURL = new URL(baseURL,rel);
				} catch (MalformedURLException e) {
					LOG.warning("Malformed rel url:" + rel);
					return null;
				}
			}
		}
		if(absURL == null) {
			try {
				baseURL = new URL(base);
				lastBase = base;
			} catch (MalformedURLException e) {
				LOG.warning("Malformed base url:" + base);
				return null;
			}
			try {
				absURL = new URL(baseURL,rel);
			} catch (MalformedURLException e) {
				LOG.warning("Malformed rel url:" + rel);
				return null;
			}
		}
		return absURL.toString();
	}
	public String evaluate(String page, String base, String url) {
		if((url == null) || (url.length() == 0)) {
			return null;
		}
		if(isAbsolute(url)) {
			return url;
		}
		if((base != null) && (base.length() > 0)) {
			String tmp = resolve(base,url);
			if(tmp != null) {
				return tmp;
			}
		}
		if((page != null) && (page.length() > 0)) {
			String tmp = resolve(page,url);
			if(tmp != null) {
				return tmp;
			}
		}
		return url;
	}
	
	

}
