package uk.ac.ox.oii.hive.udf;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDF;

//See also UDFParseUrl
//http://hive.apache.org/docs/r0.10.0/api/org/apache/hadoop/hive/ql/udf/UDFParseUrl.html
//Possible values are HOST,PATH,QUERY,REF,PROTOCOL,AUTHORITY,FILE,USERINFO 

@Description(name="DomainPart",
value="_FUNC_(STRING, INT) - from the input url string returns the input int domain part of the url.",
extended = "Returns NULL if url is invalid.\nExample:\n   > SELECT _FUNC_('http://www.oii.ox.ac.uk/people/hale/',3); \n 'ox.ac.uk'"		
)

public class DomainPart extends UDF{
	
	public DomainPart() {
		super();
	}
	
	public String evaluate(String domain) {
		return evaluate(domain,0);
	}
	
	public String evaluate(String domain, Integer part) {
		int p = 0;
		if (part!=null) {
			p=part.intValue();
		}
		return evaluate(domain,p);
	}
		
	public String evaluate(String domain, int part) {
		
		String domainClean = getDomain(domain);
		
		String[] domainParts = domainClean.split("\\.");
		StringBuilder sb = new StringBuilder();
		
		int start = domainParts.length-part;
		if (part==0 || start<=0) {
			return domainClean;
		} else {
			sb.append(domainParts[start]);
			for (int i=start+1; i<domainParts.length; i++) {
				sb.append(".").append(domainParts[i]);
			}			
		}
		
		
		return sb.toString();
	}
	
	
	protected static String getDomain(String url) {
		if (url==null) {
			return "";
		}
		int start = url.indexOf("://");
		if (start!=-1) {
			start+=3;
		} else {
			start=0;
		}
		
		int end = url.indexOf(":",start);//port
		if (end==-1) {
			end=url.length();
		}
		
		int newEnd = url.indexOf("/",start);
		if (newEnd!=-1 && newEnd<end) {
			end=newEnd;
		}
		
		newEnd = url.indexOf("?",start);
		if (newEnd!=-1 && newEnd<end) {
			end=newEnd;
		}
		
		newEnd = url.indexOf("#",start);
		if (newEnd!=-1 && newEnd<end) {
			end=newEnd;
		}
		
		return url.substring(start,end);
	}
	

}
